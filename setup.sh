#TODO automatically install wireshark with option to disable packet capture by non-superusers
sudo apt-get update && sudo apt-get dist-upgrade -y && sudo apt-get install -y thunderbird blender cockpit deluge gpg git handbrake homebank inkscape gimp kazam make atom network-manager-openvpn network-manager-openvpn-gnome network-manager-pptp network-manager-pptp-gnome nextcloud-client krdc vlc wireshark openscad
#clone git repos
cd && git clone https://gitlab.com/libresauce/pop_os-setup.git
git clone https://gitlab.com/libresauce/poetry-and-prayers.git
git clone https://gitlab.com/libresauce/resume.git
#install fonts
sudo rsync -rvh ~/pop_os-setup/fonts/. /usr/share/fonts/truetype
